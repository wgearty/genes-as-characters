# README #

### What is this repository for? ###

* This is the bitbucket repository for Genes as Characters R script, as described by Gearty and Gauthier (in prep).
* Version 1.0.1

### How do I get set up? ###

* Make sure RAxML (https://github.com/stamatak/standard-RAxML) and PAUP* (http://people.sc.fsu.edu/~dswofford/paup_test/) are either in the working directory or in your PATH.
* The user than define which sequential and Pthreads version are used; the defaults are "raxmlHPC-AVX2", "raxmlHPC-PTHREADS-AVX2".
* Adjust the working directory, number of genes, number of taxa, partition file name, alignment file name, RAxML output prefix, and the PAUP* command at the top of the script.
* Run the entire script.

* Dependencies: RAxML and PAUP*

### Who do I talk to? ###

* [William Gearty](mailto:wgearty@stanford.edu) (author)
* Post an issue